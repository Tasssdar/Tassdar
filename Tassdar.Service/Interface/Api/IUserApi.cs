﻿using System;
using System.Collections.Generic;
using System.Text;
using Tassdar.Framework.Model.Base;
using Tassdar.Service.Entity;
using Tassdar.Service.Request;

namespace Tassdar.Service.Interface.Api
{
    public interface IUserApi
    {
        ResultModel AddUser(UserRequest entity);

        ResultModel UpdateUser(UserRequest entity);

        ResultModel Remove(int user_id);

        IList<UserEnitty> GetUserList();

        (IList<UserEnitty>,int rows_count )GetUserList(int pageIndex, int pageSize);
    }
}
