﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Tassdar.Framework.Interface.Base
{
   public static class IOCRegister
    {
        public static IServiceCollection Register(this IServiceCollection serviceCollection, params Assembly[] assemblies)
        {
            Type baseType = typeof(IDependency);
            foreach (var assembly in assemblies)
            {
                foreach (var type in assembly.GetTypes().Where(x=> baseType.IsAssignableFrom(x)).Where(x=>x.IsClass && x.IsAbstract==false))
                {
                    foreach (var interfaceType in type.GetInterfaces().Where(x=> baseType.IsAssignableFrom(x)))
                    {
                        serviceCollection.Add(new ServiceDescriptor(interfaceType, type, ServiceLifetime.Transient));
                    }
                }   
            }
            return serviceCollection;
        }
    }
}
