﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tassdar.Framework.Interface.Base
{
    /// <summary>
    /// 所有要使用DI的接口默认要实现此接口
    /// </summary>
   public interface IDependency
    {
    }
}
