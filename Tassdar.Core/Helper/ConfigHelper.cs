﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tassdar.Core.Helper
{
    /// <summary>
    /// 获取配置文件内容工具类
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// 此变量请在startup中赋值
        /// </summary>
        public static IConfiguration Configs;

        public static TVal GetValue<TVal>(string key)
        {
            var res = Utility.Parse<TVal>(Configs[key]);
            return res;
        }
    }
}
